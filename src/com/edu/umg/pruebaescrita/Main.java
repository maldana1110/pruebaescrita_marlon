package com.edu.umg.pruebaescrita;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Tv tv = new Tv();
        tv.setEsPlana(1);
        tv.setMarcaTv("LG");
        tv.setPulgadas("50 pulgadas");

        Canal canal = new Canal();
        canal.setNumCanal(2);
        canal.setPermitido(1);
        canal.setTematica("Dibujos Animados");
        tv.addCanalesPredefinidos(canal);

        canal = new Canal();
        canal.setNumCanal(3);
        canal.setPermitido(1);
        canal.setTematica("Series");
        tv.addCanalesPredefinidos(canal);

        canal = new Canal();
        canal.setNumCanal(4);
        canal.setPermitido(1);
        canal.setTematica("Dibujos Animados");
        tv.addCanalesPredefinidos(canal);

        canal = new Canal();
        canal.setNumCanal(5);
        canal.setPermitido(1);
        canal.setTematica("Deportes");
        tv.addCanalesPredefinidos(canal);

        System.out.println("Canal 2 es :" +tv.canalPermitido(2));
        System.out.println("Canal 3 es :" +tv.canalPermitido(3));
        System.out.println("Canal 4 es :" +tv.canalPermitido(4));
        System.out.println("Canal 5 es :" +tv.canalPermitido(5));
    }
}
