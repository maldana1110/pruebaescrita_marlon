package com.edu.umg.pruebaescrita;

public class Canal {
    private int numCanal;
    private String tematica;
    private int permitido; //1 Permitido 2 No Permitido

    public Canal() {
    }

    public Canal(int numCanal, String tematica, int permitido) {
        this.numCanal = numCanal;
        this.tematica = tematica;
        this.permitido = permitido;
    }

    public int getNumCanal() {
        return numCanal;
    }

    public void setNumCanal(int numCanal) {
        this.numCanal = numCanal;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public int getPermitido() {
        return permitido;
    }

    public void setPermitido(int permitido) {
        this.permitido = permitido;
    }
}