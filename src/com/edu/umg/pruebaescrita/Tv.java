package com.edu.umg.pruebaescrita;

import sun.security.pkcs.ParsingException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Tv {
    private String marcaTv;
    private String pulgadas;
    private int esPlana; //1 Es plana 0 No Plana
    private ArrayList canalesPredefinidos = new ArrayList();

    public void addCanalesPredefinidos(Canal canal)
    {
        canalesPredefinidos.add(canal);
    }

    public Tv() {
        canalesPredefinidos = new ArrayList();
    }

    public Tv(java.lang.String marcaTv, java.lang.String pulgadas, int esPlana, ArrayList canalesPredefinidos) {
        this.marcaTv = marcaTv;
        this.pulgadas = pulgadas;
        this.esPlana = esPlana;
        this.canalesPredefinidos = canalesPredefinidos;
    }

    public String getMarcaTv() {
        return marcaTv;
    }

    public void setMarcaTv(String marcaTv) {
        this.marcaTv = marcaTv;
    }

    public String getPulgadas() {
        return pulgadas;
    }

    public void setPulgadas(String pulgadas) {
        this.pulgadas = pulgadas;
    }

    public int getEsPlana() {
        return esPlana;
    }

    public void setEsPlana(int esPlana) {
        this.esPlana = esPlana;
    }

    public ArrayList getCanalesPredefinidos() {
        return canalesPredefinidos;
    }

    public void setCanalesPredefinidos(ArrayList canalesPredefinidos) {
        this.canalesPredefinidos = canalesPredefinidos;
    }

    public boolean canalPermitido(int canal) {
        boolean permitido = false; //1 Es Permitido 0 No es Permitido
        for(int i = 0 ; i < canalesPredefinidos.size() ; i++) {
            Canal canal2 = (Canal) canalesPredefinidos.get(i);
            if(canal2.getNumCanal() == canal && canal2.getTematica().equals("Dibujos Animados")) {
                permitido = true;
            }
        }
        return permitido;
    }
}
